package pl.sages.calc;

import org.junit.Assert;
import org.junit.Test;

public class ScienceCalculatorTest {

    @Test
    public void test2plus2(){
        ScienceCalculator c = new ScienceCalculator();
        c.add(2,2);
        Assert.assertEquals(4, c.getResult(), 0.0);
    }

    @Test
    public void testNegativeExponent(){
        ScienceCalculator c = new ScienceCalculator();
        try {
            c.pow(-2);
            Assert.fail("exception expected");
        }catch (CalculatorException e){
        }
    }

}
