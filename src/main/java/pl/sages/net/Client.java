package pl.sages.net;

import java.io.*;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Client {

    public static void main(String[] args) {
        System.out.println("Client.main");

        ExecutorService es = Executors.newFixedThreadPool(2);
        try {
            Socket s = new Socket("localhost", 777);
            System.out.println("connected to the server.");

            BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream()));
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));

            es.execute(new MessageReader(br));
            es.execute(new MessageWriter(bw));


        } catch (IOException e) {
            e.printStackTrace();
        }

        es.shutdown();

    }
}
