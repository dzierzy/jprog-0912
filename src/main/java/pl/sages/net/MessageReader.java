package pl.sages.net;

import java.io.BufferedReader;
import java.io.IOException;

public class MessageReader implements Runnable {

    BufferedReader reader;

    public MessageReader(BufferedReader reader) {
        this.reader = reader;
    }

    @Override
    public void run() {

        try(BufferedReader br = reader) {
            String line = null;
            while ((line = reader.readLine()) != null && !line.equals("quit")) {
                System.out.println("received: " + line);
            }

            System.out.println("reader quitting...");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
