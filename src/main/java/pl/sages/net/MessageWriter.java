package pl.sages.net;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Scanner;

public class MessageWriter implements Runnable {

    private BufferedWriter writer;

    public MessageWriter(BufferedWriter writer) {
        this.writer = writer;
    }

    @Override
    public void run() {

        Scanner scanner = new Scanner(System.in);
        String msg = null;

        try(BufferedWriter bw = writer) {
            while (!"quit".equals(msg)) {
                msg = scanner.nextLine();
                bw.write(msg + "\n");
                bw.flush();
            }
            System.out.println("writer quitting...");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
