package pl.sages.xml.sax;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


public class StudentsHandler extends DefaultHandler {


    boolean student = false;

    boolean firstName = false;

    @Override
    public void startElement(String uri,
                             String localName, String qName, Attributes attributes)
            throws SAXException {

        switch(qName){
            case "student":
                student = true;
                System.out.println("id=" + attributes.getValue("id"));
                break;
            case "firstName":
                firstName = true;
                break;
        }





    }

    @Override
    public void endElement(String uri,
                           String localName, String qName) throws SAXException {

        switch(qName){
            case "student":
                student = false;
                break;
            case "firstName":
                firstName = false;
                break;
        }


    }

    @Override
    public void characters(char[] ch,
                           int start, int length) throws SAXException {

        if(student && firstName){
            System.out.println("first name: " + new String(ch, start, length));
        }

    }
}