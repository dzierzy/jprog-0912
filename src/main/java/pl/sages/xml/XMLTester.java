package pl.sages.xml;


import pl.sages.xml.dom.DOMTrainingParser;
import pl.sages.xml.sax.SaxTrainingParser;
import pl.sages.xml.xpath.XPathStudents;
import pl.sages.xml.xsl.XSLStudentsTransform;

public class XMLTester {
    public static void main(String[] args) throws Exception {

        System.out.println("<< DOM >>");
        new DOMTrainingParser().parseStudents();

        System.out.println("<< SAX >>");
        new SaxTrainingParser().parseStudents();

        System.out.println("<< XPATH >>");
        new XPathStudents().parseStudents();

        System.out.println("<< XSLT >>");
        new XSLStudentsTransform().transformStudents();


        // JAXB - Java Architecture for XML Binding
        // Jackson - fasterxml.com
    }
}
