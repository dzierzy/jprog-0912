package pl.sages.xml.dom;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;


public class DOMTrainingParser {

    Document doc;

    String date;

    public Document getDocument() {
        return doc;
    }

    public DOMTrainingParser() throws Exception {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        doc = builder.parse(new File("./src/main/resources/training.xml"));
        date = doc.getDocumentElement().getAttribute("date");
        System.out.println("Training session attendees at " + date );

    }

    public String getDate() {
        return date;
    }

    public void parseStudents(){
        NodeList students = doc.getElementsByTagName("student");
        presentStudents(students);
    }

    public void presentStudents(NodeList students){
        for(int i = 0 ; i < students.getLength(); i++){
            Node student = students.item(i);
           // TODO display id, first and last name
            Element e = (Element) student;
            System.out.println("id=" + e.getAttribute("id"));
            NodeList children = e.getChildNodes();
            for(int j=0; j<children.getLength();j++){
                if(children.item(j).getNodeType()==Node.ELEMENT_NODE) {
                    Element child = (Element) children.item(j);
                    if (child.getTagName().equals("firstName")) {
                        System.out.println("first name: " + child.getTextContent());
                    } else if (child.getTagName().equals("lastName")) {
                        System.out.println("last name: " + child.getTextContent());
                    }
                }
            }

        }
    }
}
