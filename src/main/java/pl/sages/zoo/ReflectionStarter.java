package pl.sages.zoo;

import java.io.IOException;
import java.lang.reflect.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class ReflectionStarter {

    public static void main(String[] args) {
        System.out.println("ReflectionStarter.main");

        // reflection API - java.lang.reflect

        Object o = new Horse("Kasztanka", 88);

        Class clazz = o.getClass();
        do {
            printClassInfo(clazz);
            clazz = clazz.getSuperclass();
        }while (clazz!=null);

        Path file = Paths.get("class.txt");
        try {
            List<String> lines = Files.readAllLines(file);
            String className = lines.get(0);
            String name = lines.get(1);
            int size = Integer.parseInt(lines.get(2));
            clazz = Class.forName(className);
            if(clazz.isAnnotationPresent(Predator.class)){
                System.out.println("Warning! about to instantiate predator!");
            }
            
            Constructor c = clazz.getConstructor(String.class, int.class);
            o = c.newInstance(name, size);
            if(o instanceof Animal){
                Animal a = (Animal) o;
                System.out.println("a = " + a);

                Field f = a.getClass().getSuperclass().getDeclaredField("size");
                f.setAccessible(true);
                f.set(a, 123);
                System.out.println("a = " + a);

            } else {
                throw new IllegalStateException("must be animal");
            }
            
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }


        System.out.println("done.");
    }

    private static void printClassInfo(Class clazz) {
        System.out.println("*************************");
        System.out.println("class name: " + clazz.getSimpleName() + ", package:" + clazz.getPackage().getName());

        System.out.println("interfaces: " + toString(clazz.getInterfaces()));

        Constructor[] constructors = clazz.getConstructors();
        for(Constructor c : constructors){
            System.out.println("constructor: " + c.getName() + ", params: " + toString(c.getParameterTypes()));
        }

        Method[] methods = clazz.getDeclaredMethods();
        for(Method m : methods){
            System.out.println(
                    "method: " + m.getName() +
                            ", param:" + toString(m.getParameterTypes()) +
                            " returns " + m.getReturnType().getName() +
                    ", modifiers:" + modifiers(m.getModifiers()));
        }

        Field[] fields = clazz.getDeclaredFields();
        for(Field f : fields){
            System.out.println(
                    "field: " + f.getName() + " type " + f.getType().getName() + ", modifiers:" + modifiers(f.getModifiers()));
        }
    }

    public static String modifiers(int m){
        StringBuilder sb = new StringBuilder();
        if(Modifier.isPublic(m)){
            sb.append("public,");
        } else if(Modifier.isProtected(m)){
            sb.append("protected,");
        } else if(Modifier.isPrivate(m)){
            sb.append("private,");
        } else if(Modifier.isAbstract(m)){
            sb.append("abstract,");
        } else if(Modifier.isStatic(m)){
            sb.append("static,");
        } else if(Modifier.isFinal(m)){
            sb.append("final,");
        }
        return sb.toString();
    }

    public static String toString(Class[] classes){

        StringBuilder sb = new StringBuilder();
        for(Class param : classes){
            sb.append(param.getName() + ",");
        }
        return sb.toString();
    }

}
