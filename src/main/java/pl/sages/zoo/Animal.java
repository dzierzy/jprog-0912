package pl.sages.zoo;

public abstract class Animal implements Comparable<Animal>{

    private String name;

    private int size;

    public Animal(String name, int size) {
        this.name = name;
        this.size = size;
    }

    public void eat(){
        System.out.println("animal is eating...");
    }

    public abstract void move();

    public String getName() {
        return name;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public int compareTo(Animal a) {
        return a.size - this.size;
    }

    @Override
    public String toString() {
        return "Animal{" +
                "name='" + name + '\'' +
                ", size=" + size +
                '}';
    }
}
