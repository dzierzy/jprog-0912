package pl.sages.zoo;

@Predator
public class Shark extends Animal {

    public Shark(String name, int size) {
        super(name, size);
    }

    @Override
    public void move() {
        System.out.println("sharki is swimming...");
    }
}
