package pl.sages.zoo;

import pl.sages.travel.Transportation;

public class Horse extends Animal implements Transportation {

    public Horse(String name, int size) {
        super(name, size);
    }
    @Override
    public void move() {
        System.out.println("horse is runnign very fast...");
    }

    @Override
    public void transport(String passenger) {
        System.out.println("passenger " + passenger + " is traveling by horse.");
        move();
    }
}
