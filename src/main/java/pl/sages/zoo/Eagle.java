package pl.sages.zoo;

@Predator
public class Eagle extends Animal {


    public Eagle(String name, int size) {
        super(name, size);
    }

    @Override
    public void move() {
        System.out.println("eagle is flying...");
    }

    @Override
    public void eat() {
        System.out.println("eagle is eating another animal...");
    }
}
