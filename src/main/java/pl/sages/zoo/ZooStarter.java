package pl.sages.zoo;


import java.util.*;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class ZooStarter {

    public static void main(String[] args) {
        System.out.println("ZooStarter.main");

        Animal a = new Horse("Piorun", 150);
        a.eat();
        a.move();
        
        Box<Animal> animalBox = new Box<>();
        animalBox.setObject(a);
        Animal animalFromBox = animalBox.getObject();
        System.out.println("animalFromBox = " + animalFromBox);
        

        List<Animal> animals = new ArrayList<>();
        animals.add(a);
        animals.add(new Eagle("Bielik", 10));
        animals.add(new Shark("Jerry", 30));
        animals.add(new Eagle("Jerry", 18));
        animals.add(a);

        /*animals.sort((a1, a2) -> a1.getName().compareTo(a2.getName()));
        animals.removeIf(o -> o.getName().equals("Jerry"));
        animals.forEach(o -> System.out.println("consumer, animal: " + o));*/

        // Stream API =/= data streams
        Stream<? extends Animal> stream = Stream.iterate(new Horse("1", 1), o -> new Horse(o.getName(), o.getSize())).limit(5000);

        Consumer<Animal> consumer = System.out::println;
                //o -> System.out.println(o);

        //animals =
        //long count =
        //List<String> names = 
        //boolean match =
        Optional<String> optionalName =
        stream.
                sorted((a1, a2) -> a1.getName().compareTo(a2.getName())) // Comparator
                .filter(o -> !o.getName().equals("Jerry")) // Predicate
                .peek(consumer) // Consumer
                //.limit(2);
                .map(o->o.getName()) // Function
                //.distinct()
                .findFirst();
                //.allMatch(n->n.contains("o"));
                //.collect(Collectors.toList()); // terminla method
                //.count();



        Supplier<RuntimeException> supplier = IllegalArgumentException::new;
                //() -> new IllegalArgumentException();
        String name = optionalName.orElseThrow(supplier);
        System.out.println("name = " + name);


        /*Collections.sort(animals, (a1, a2) -> a1.getName().compareTo(a2.getName()));

        System.out.println("animals count: " + animals.size());
        for(Animal animal : animals){
            System.out.println(animal);
        }

        Iterator<Animal> itr = animals.iterator();
        while(itr.hasNext()){
            Animal animal = itr.next();
            if(animal.getName().equals("Jerry")){
                itr.remove();
            }
        }*/

        // google guava -> BiMap -> reverse()
        Map<Employee, Animal> assignement = new HashMap<>();
        assignement.put(new Employee("Jan", "Kowalski"), new Eagle("Bielik2", 12));
        assignement.put(new Employee("Adam", "Nowak"), new Shark("Joe", 38));
        assignement.put(new Employee("Krzysztof", "Iksinski"), new Horse("Blyskawica", 150));
        assignement.put(new Employee("Adam", "Nowak"), new Horse("Kasztanka", 99));

        /*
        boolean same = false;
         int hash1 = o1.hashCode();
         int hash2 = o2.hashCode();
         if(hash1==hash2){
              same = o1.equals(o2);
              }

         */


        Animal nowakAnimal = assignement.get(new Employee("Adam", "Nowak"));
        System.out.println("nowakAnimal = " + nowakAnimal);

        for(Map.Entry<Employee, Animal> entry: assignement.entrySet()){
            System.out.println("key:" + entry.getKey() + ", value: " + entry.getValue());
        }

        for( Employee key : assignement.keySet()){
            System.out.println("key: " + key + ", value: " + assignement.get(key));
        }


        System.out.println("done.");
    }
}
