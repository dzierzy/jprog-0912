package pl.sages.zoo;

import java.util.Comparator;

public class AnimalComparator implements Comparator<Animal> {

    @Override
    public int compare(Animal a1, Animal a2) {
        int result = a1.getName().compareTo(a2.getName());
        if(result==0){
            return a2.getSize() - a1.getSize();
        } else {
            return result;
        }
    }
}
