package pl.sages.travel;

public class Train implements Transportation {

    @Override
    public void transport(String passenger) {
        System.out.println("passenger " + passenger + " is traveling by train.");
    }
}
