package pl.sages.travel;

@FunctionalInterface
public interface Transportation {


    public static final String description = "an object to transport somebody";

    void transport(String passenger);

    default int getSpeed(){
        transport("fake");
        return 99;
    }

    static String getDescription(){
        return "transportation description";
    }
}
