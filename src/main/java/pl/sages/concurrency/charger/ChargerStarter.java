package pl.sages.concurrency.charger;

import pl.sages.concurrency.ThreadNamePrefixPrintStream;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ChargerStarter {

    public static void main(String[] args) {

        System.setOut(new ThreadNamePrefixPrintStream(System.out));

        ExecutorService es = Executors.newFixedThreadPool(2);

        Electricity.getInstance().turnOn();

        Charger samsungCharger = new Charger();
        Charger appleCharger = new Charger();

        Phone samsung = new Phone(50, "Samsung S11");
        Phone apple = new Phone(35, "Iphone XR");

        Runnable r1 = () -> samsungCharger.chargeDevice(samsung, 8);
        Runnable r2 = () -> appleCharger.chargeDevice(apple, 10);
        es.execute(r1);
        es.execute(r2);

        es.shutdown();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Electricity.getInstance().turnOn();


        System.out.println("done.");



    }
}
