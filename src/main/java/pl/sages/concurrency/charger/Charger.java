package pl.sages.concurrency.charger;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Charger {

    private static final int POWER = 3;

    public void chargeDevice(Phone p, int hours) {
        p.charge(hours*POWER);
    }
}
