package pl.sages.concurrency.account;


import java.util.concurrent.atomic.AtomicInteger;

public class Account {

    private AtomicInteger balance = new AtomicInteger(1_000_000);

    public boolean withdraw(int amount) {

            if (balance.get() >= amount) {
            /*
            1. read balance
            2. calculate
            3. assign
             */
                balance.addAndGet(-amount);
                //balance = balance - amount;
                return true;
            } else {
                return false;
            }

    }

    public  void deposit(int amount) {
        /*
            1. read balance
            2. calculate
            3. assign
             */

            balance.addAndGet(amount);

    }

    public int getBalance() {
        return balance.get();
    }
}
