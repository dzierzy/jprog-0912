package pl.sages.concurrency.account;

import pl.sages.concurrency.ThreadNamePrefixPrintStream;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class ThreadsStart {

    public static void main(String[] args) {

        System.setOut(new ThreadNamePrefixPrintStream(System.out));

        System.out.println("ThreadsStart.main");
        Account account = new Account();

        // TODO deposit 1000000 times
        // TODO withdraw 1000000 times

        ExecutorService es = Executors.newFixedThreadPool(2);

        DepositTask depositTask = new DepositTask(account);
        WithdrawTask withdrawTask = new WithdrawTask(account);
        es.execute(depositTask);
        es.execute(withdrawTask);

        es.shutdown();



        // expected 1_000_000



        System.out.println("done.");


    }

}
