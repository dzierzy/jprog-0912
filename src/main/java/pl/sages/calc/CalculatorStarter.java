package pl.sages.calc;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class CalculatorStarter {

    public static void main(String[] args) {
        System.out.println("CalculatorStarter.main");

        System.out.println("2+2=" + Calculator.multiply(2,2));

        Calculator c = new Calculator(2);
        c.load();

        Calculator c2 = new ScienceCalculator();

        c.add(1);

        c.subtract(1.5);
        c.divide(1.1);
        c.multiply(3.2);
        System.out.println("result=" + c.getResult());
        c.save();

        c2.add(2);
        c2.multiply(2);

        if(c2 instanceof ScienceCalculator) {
           try {
               ((ScienceCalculator) c2).pow(3);
           }catch (CalculatorException e){
               e.printStackTrace();
               return;
           } finally {
               System.out.println("in finally");
           }

        }


        Locale locale = new Locale("pl", "PL");
        DateFormat format = new SimpleDateFormat("yyyy*DD*MM G E X", locale);                //
        Date date = new Date();

        //LocalDate localDate = LocalDate.of(2020, 1, 1);
        //LocalTime localTime = LocalTime.now();
        LocalDateTime localDateTime = LocalDateTime.now();

        ZoneId zone = ZoneId.of("Europe/Warsaw");
        ZonedDateTime zonedDateTime = localDateTime.atZone(zone);

        zone = ZoneId.of("Asia/Singapore");

        zonedDateTime = zonedDateTime.withZoneSameInstant(zone);

        String timestamp = zonedDateTime.format(DateTimeFormatter.ofPattern("yyyy/MM/dd hh:mm:ss X"));

        System.out.println(timestamp + " c2 result = " + c2);

        Date parsedDate = null;
        try {
            parsedDate = format.parse("2019*344*12 n.e. Wt +01");
        } catch (ParseException e) {
            e.printStackTrace();
            //parsedDate = new Date();
            return;
        }


        Calendar calendar = Calendar.getInstance();
        calendar.setTime(parsedDate);
        //calendar.set(2019, 2, 10);
        //calendar.roll(Calendar.MONTH, 20);
        parsedDate = calendar.getTime();

        System.out.println("parse: " + parsedDate);

        System.out.println("done.");
    }

}
