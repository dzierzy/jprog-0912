package pl.sages.calc;

public class ScienceCalculator extends Calculator {


    public ScienceCalculator(){
        super(0.0);
        System.out.println("science calculator constructor");
    }

    /**
     * ower of result
     * use Math.pow() instead
     * @param n - exponent
     */
    @Deprecated
    public void pow(int n){
        // 3 do 4 (n) -> 3*3*3*3
        // TODO handle negative n
        if(n<0){
            throw new CalculatorException("negative n not allowed: " + n);
        }
        if(n==0){
            result = 1;
            return;
        }
        double temp = result;
        for(int i=1; i<n; i++){
            result = result * temp;
        }
    }


    @Override
    public String toString() {
        return "ScienceCalculator{" +
                "result=" + result +
                '}';
    }
}
