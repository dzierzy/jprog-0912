package pl.sages.calc;

// Error, RuntimeException -> unchecked
// Exception (except RuntimeException) -> checked
public class CalculatorException extends RuntimeException {

    public CalculatorException(String message) {
        super(message);
    }

    public CalculatorException(String message, Throwable cause) {
        super(message, cause);
    }
}
