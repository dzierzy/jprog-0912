package pl.sages.calc;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.util.Date;


public class Calculator /*extends Object*/ implements Serializable{

   // fields
    protected double result = 0.0;




    // constructors
    public Calculator(double result){
        this.result = result;
    }


    private File init() throws IOException {

        File dir = new File("calc");
        if (!dir.exists()) {
            System.out.println("creating dir " + dir.getCanonicalPath());
            dir.mkdirs();
        } else {
            System.out.println("dir " + dir.getCanonicalPath() + " already exists");

            File[] files = dir.listFiles((d,name) ->  name.startsWith("calc."));
            for(File f : files){
                System.out.println("file: " + f.getCanonicalPath() + ", is directory:" + f.isDirectory());
            }
        }

        File file = new File(dir, "calc.txt");
        if(!file.exists()){
            System.out.println("creating file " + file.getCanonicalPath());
            file.createNewFile();
        } else {
            System.out.println("file " + file.getCanonicalPath() + " already exists");
        }

        return file;
    }

    private File initNIO2() throws IOException {
        Path dir = Paths.get("calc");
        if(!Files.exists(dir)){
            System.out.println("creating dir " + dir);
            Files.createDirectories(dir);
        } else {
            System.out.println("dir " + dir + " already exists");
        }

        Path file = Paths.get(dir.toString(), "calc.txt");
        if(!Files.exists(file)){
            System.out.println("creating file " + file);
            Files.createFile(file);
        } else {
            System.out.println("file " + file + " already exists");
        }

        return file.toFile();
    }

    private Connection getConnection(){
        String driverClassName = "org.h2.Driver";
        String jdbcUrl = "jdbc:h2:tcp://localhost/~/test";
        String user = "sa";
        String password = "";

        try {
            Class.forName(driverClassName);
            return DriverManager.getConnection(jdbcUrl, user, password);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return null;

    }

    public void load(){

        try(Connection c = getConnection()){

            System.out.println("connected.");
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT ID, RESULT, CREATED FROM MEMORY ORDER BY ID DESC LIMIT 1");

            if(rs.next()){
                int id = rs.getInt("ID");
                double result = rs.getDouble("RESULT");
                Date created = rs.getDate("CREATED");
                System.out.println("id=" + id + ", result=" + result + ", created=" + created);
                this.result = result;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

   public void loadFromFile(){
       // TODO load result from a file
       try(BufferedReader br = new BufferedReader(new FileReader(init()))){

           String line = null;
           String last = null;
           while( (line=br.readLine())!=null){
               System.out.println("line: " + line);
               last = line;
           }
           if(last!=null) {
               result = Double.parseDouble(last); // autoboxing -> primitive to object conversion, unboxing object to primitive
           }

       } catch (IOException e) {
           e.printStackTrace();
       }

   }

   public void save(){

        try(Connection c = getConnection()){
            try {
                c.setAutoCommit(false);
                c.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
                PreparedStatement stmt = c.prepareStatement("INSERT INTO MEMORY(RESULT,CREATED VALUES (?, SYSDATE())");
                stmt.setDouble(1, result);
                int count = stmt.executeUpdate();
                System.out.println(count + " row(s) updated");
                c.commit();
            } catch (SQLException e){
                c.rollback();
                throw e;
            }
        } catch (SQLException e) {
            e.printStackTrace();

        }
   }

   public void saveToFile(){
       // try-with-resources
       try(BufferedWriter bw = new BufferedWriter(new FileWriter(init(), true))) {
           bw.write(Double.toString(result) + "\n");
       } catch (IOException e) {
           e.printStackTrace();
       }
   }


    // business methods
    public void add(double first, double... operand){
        result = result + first;
        for( double o : operand) {
            result = result + o;
        }
    }

    public void subtract(double operand){
        result = result - operand;
    }

    public void multiply(double operand){
        result = result * operand;
    }

    public void divide(double operand){
        result = result / operand;
    }

    // technical methods
    public double getResult() {
        return this.result;
    }


    public static int multiply(int a, int b){
        return a * b;
    }

}
