package pl.sages.calc;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class SerializationStarter {

    public static void main(String[] args) {
        System.out.println("Let's serialize!");

        Calculator c = new Calculator(3.3);

        try(ObjectOutputStream oos =
                       new ObjectOutputStream(new FileOutputStream(new File("calc/calc.serialized")));){
            oos.writeObject(c);
        }  catch (IOException e) {
            e.printStackTrace();
        }

        /*try(ObjectInputStream ois =
                new ObjectInputStream(new FileInputStream(new File("calc/calc.serialized")))){
            Object o = ois.readObject();
            System.out.println("o = " + o);
        }  catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }*/

    }
}
